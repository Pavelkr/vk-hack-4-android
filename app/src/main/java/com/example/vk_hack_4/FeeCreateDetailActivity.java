package com.example.vk_hack_4;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class FeeCreateDetailActivity extends AppCompatActivity {
    Spinner feeCreateAuthorSpinner;
    EditText feeCreateToDate;
    TextView feeCreateToDateTextView;
    Button saveButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_create_detail);

        Intent intent = getIntent();
        final String feeHeader = intent.getStringExtra("feeName");
        final String feeSumm = intent.getStringExtra("feeSumm");
        final String feeTarget = intent.getStringExtra("feeTarget");
        final String feeDescription = intent.getStringExtra("feeDescription");
        final String feeAuthor = intent.getStringExtra("feeAuthor");

        /*
        Поменяем заголовок
         */
        getSupportActionBar().setTitle("Дополнительно");
        /*
        * Активируем кнопку назад
        * */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        /*
         * Спиннер с автором
         */
        feeCreateAuthorSpinner = findViewById(R.id.fee_create_author_spinner);
        String[] payTypes = {"Кривошеев Павел", "Сообщество \"Помощь зверям\""};
        ArrayAdapter<String> payTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, payTypes);
        payTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feeCreateAuthorSpinner.setAdapter(payTypeAdapter);

        /*
        Обработчик переключения радио баттонов
         */

        feeCreateToDate = findViewById(R.id.fee_create_to_date);
        feeCreateToDateTextView = findViewById(R.id.fee_create_to_date_text_view);

        RadioGroup yourRadioGroup = (RadioGroup) findViewById(R.id.createDetailRadio);

        yourRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.radio_to_end:
                        feeCreateToDate.setVisibility(View.GONE);
                        feeCreateToDateTextView.setVisibility(View.GONE);
                        break;
                    case R.id.radio_to_date:
                        feeCreateToDate.setVisibility(View.VISIBLE);
                        feeCreateToDateTextView.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        saveButton = findViewById(R.id.create_fee_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    //Иначе переход на результирующий экран
                    Intent intent = new Intent(FeeCreateDetailActivity.this, FeeResultActivity.class);
                    intent.putExtra("feeName", feeHeader.toString());
                    intent.putExtra("feeSumm", feeSumm.toString());
                    intent.putExtra("feeTarget", feeTarget.toString());
                    intent.putExtra("feeDescription", feeDescription.toString());
                    intent.putExtra("feeAuthor", feeCreateAuthorSpinner.getSelectedItem().toString());
                    startActivity(intent);
                }
        });
    }

    /*
    Обраотка кнопки назад
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}