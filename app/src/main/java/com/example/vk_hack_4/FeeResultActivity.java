package com.example.vk_hack_4;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class FeeResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_result);

        TextView feeNameTextView = (TextView) findViewById(R.id.fee_header);
        TextView feeAuthorTextView = (TextView) findViewById(R.id.fee_author);
        TextView feeTargetTextView = (TextView) findViewById(R.id.fee_target);
        TextView feeProgressTitleTextView = (TextView) findViewById(R.id.progress_title);

        Intent intent = getIntent();
        String feeHeader = intent.getStringExtra("feeName");
        String feeSumm = intent.getStringExtra("feeSumm");
        String feeTarget = intent.getStringExtra("feeTarget");
        String feeDescription = intent.getStringExtra("feeDescription");
        String feeAuthor = intent.getStringExtra("feeAuthor");


        feeNameTextView.setText(feeHeader);
        feeAuthorTextView.setText("Автор " + feeAuthor);
        feeTargetTextView.setText(feeTarget);
        feeProgressTitleTextView.setText("Нужно собрать " + feeSumm);


        /*
        Поменяем заголовок
         */
        getSupportActionBar().setTitle("Целевой сбор");


        /*
        * Активируем кнопку назад
        * */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /*
    Обраотка кнопки назад
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}