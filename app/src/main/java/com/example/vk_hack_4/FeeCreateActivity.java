package com.example.vk_hack_4;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class FeeCreateActivity extends AppCompatActivity {

    Button nextButton;
    Spinner feeCreateSpinner;
    Spinner feeCreateAuthorSpinner;
    LinearLayout authorBlock;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_create);
        /*
        * Режим добавления записи
        * */
        Intent intent = getIntent();

        final String mode = intent.getStringExtra("mode");
        authorBlock = findViewById(R.id.author_block);


        if (mode != null && mode.equals("regular")) {
            authorBlock.setVisibility(View.VISIBLE);
        }

        feeCreateAuthorSpinner = findViewById(R.id.fee_create_author_spinner);

        /*
        Поменяем заголовок
         */
        getSupportActionBar().setTitle("Целевой сбор");
        /*
        * Активируем кнопку назад
        * */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        /*
        * Далее
        * */
        nextButton = findViewById(R.id.fee_create_next);
        feeCreateSpinner = findViewById(R.id.fee_create_spinner);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //если сбор не регулярный, то нужно указать даты сбора
                //Собираем поля формы
                EditText feeName = (EditText) findViewById(R.id.fee_name);
                EditText feeSumm = (EditText) findViewById(R.id.fee_summ);
                EditText feeTarget = (EditText) findViewById(R.id.fee_target);
                EditText feeDescription = (EditText) findViewById(R.id.fee_description);

                Boolean allowSend = true;

                if (feeName.getText().toString().equals("")) {
                    Toast.makeText(FeeCreateActivity.this, "Заполните поле название сбора", Toast.LENGTH_SHORT).show();
                    allowSend = false;
                } else if (feeSumm.getText().toString().equals("")) {
                    Toast.makeText(FeeCreateActivity.this, "Заполните поле сумма сбора", Toast.LENGTH_SHORT).show();
                    allowSend = false;
                } else if (feeTarget.getText().toString().equals("")) {
                    Toast.makeText(FeeCreateActivity.this, "Заполните поле цель сбора", Toast.LENGTH_SHORT).show();
                    allowSend = false;
                } else if (feeDescription.getText().toString().equals("")) {
                    Toast.makeText(FeeCreateActivity.this, "Заполните поле описание", Toast.LENGTH_SHORT).show();
                    allowSend = false;
                }
                if (mode != null && !mode.equals("regular") && allowSend) {
                    Intent intent = new Intent(FeeCreateActivity.this, FeeCreateDetailActivity.class);
                    intent.putExtra("feeName", feeName.getText().toString());
                    intent.putExtra("feeSumm", feeSumm.getText().toString());
                    intent.putExtra("feeTarget", feeTarget.getText().toString());
                    intent.putExtra("feeDescription", feeDescription.getText().toString());
                    intent.putExtra("feeAuthor", feeCreateAuthorSpinner.getSelectedItem().toString());
                    startActivity(intent);
                } else if (allowSend) {
                    //Иначе переход на результирующий экран
                    Intent intent = new Intent(FeeCreateActivity.this, FeeResultActivity.class);
                    intent.putExtra("feeName", feeName.getText().toString());
                    intent.putExtra("feeSumm", feeSumm.getText().toString());
                    intent.putExtra("feeTarget", feeTarget.getText().toString());
                    intent.putExtra("feeDescription", feeDescription.getText().toString());
                    intent.putExtra("feeAuthor", feeCreateAuthorSpinner.getSelectedItem().toString());
                    startActivity(intent);
                }
            }
        });

        /*
        * Спиннер с оплатой
        * */
        String[] payTypes = {"Счёт VK Pay · 1234", "Банковская карта · *7493"};
        ArrayAdapter<String> payTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, payTypes);
        payTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feeCreateSpinner.setAdapter(payTypeAdapter);

        /*
         * Спиннер с автором
         */
        String[] authors = {"Кривошеев Павел", "Сообщество \"Помощь зверям\""};
        ArrayAdapter<String> authorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, authors);
        authorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feeCreateAuthorSpinner.setAdapter(authorAdapter);
    }
    /*
    Обраотка кнопки назад
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}