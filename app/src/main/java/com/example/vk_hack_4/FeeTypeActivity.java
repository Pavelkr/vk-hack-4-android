package com.example.vk_hack_4;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class FeeTypeActivity extends AppCompatActivity {
    RelativeLayout layoutDate;
    RelativeLayout layoutRegular;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_type);
        /*
        Поменяем заголовок
         */
        getSupportActionBar().setTitle("Тип сбора");
        /*
        * Активируем кнопку назад
        * */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        /*
        Сбор к дате
         */
        layoutDate = findViewById(R.id.layout_target);

        layoutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FeeTypeActivity.this, FeeCreateActivity.class);
                intent.putExtra("mode", "target");
                startActivity(intent);
            }
        });

        /*
        Сбор регулярный
         */
        layoutRegular = findViewById(R.id.layout_regular);

        layoutRegular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FeeTypeActivity.this, FeeCreateActivity.class);
                intent.putExtra("mode", "regular");
                startActivity(intent);
            }
        });

    }
    /*
    Обраотка кнопки назад
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}